from fastapi import APIRouter, File, UploadFile
from fastapi.responses import HTMLResponse
import shutil
import pytesseract

router = APIRouter()



@router.get("/", response_class=HTMLResponse)
async def home():
    return """
        <html>
            <body>
                <form action="/ocr" enctype="multipart/form-data" method="post">
                    <input name="image" type="file">
                    <input type="submit" value="Upload">
                </form>
            </body>
        </html>
    """

@router.post('/ocr')
def ocr(image: UploadFile = File(...)):
    filePath = 'txtFile'
    with open(filePath, "w+b") as buffer:
        shutil.copyfileobj(image.file, buffer)
    return pytesseract.image_to_string(filePath, lang='eng')
